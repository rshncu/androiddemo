# AndroidDemo

 ✏️ **Task**:
 
- A simple testcase can be found at [MyEspressoTest](https://bitbucket.org/rshncu/androiddemo/src/master/app/src/androidTest/java/com/mytaxi/android_demo/MyEspressoTest.java).
- The CircleCI builds [here](https://circleci.com/bb/rshncu/androiddemo/tree/master)

📖 **Rules/Comments**:

- I must confess that I had never worked with this framework, nor had I touched Android code. It was a really challenge for me and I had a lot of fun. The Firebase experience was really fun.
- I used Java, I dont have experience with Kotlin.
- I would like to do better test code, dividing the test in atomic testcases, unfortunately I dont have enough time :(
	- Login
	- Logout
	- Search driver
	- Show driver profile
- With more time I could find the way to avoid the explicit wait (*Thread.sleep()*) using the [Espresso Idling Resources](https://developer.android.com/training/testing/espresso/idling-resource) in the app code.
- Recomendations for PO:
	- When the driver info is shown the top notification bar is covered, it could be important for the customer see the time just in the moment that she is trying to contact a taxi driver.
	- The driver information shown should be usefull to make the decition about call or not, since it is the only action that can be done.
	- There is a possible bug, steps to reproduce: 
		- Login with a valid credential.
		- Keep a touch on *Search driver here* until the cursor mark or contextual men appears.
		- Touch the burguer icon to show the navigation view
		- Possible unexpected behavior: the cursor mark can be seen over the navigation view
		
Thank you very much for the opportunity, I had a lot of fun with the task. 
I am so sorry, for family reasons I have not been able to devote as much time as I would have liked. 
Looking forward for next step.	
